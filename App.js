import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screens/HomeScreen'
import UserDetailScreen from './screens/UserDetailScreen'

import AddScreen from './screens/AddScreen'
import Axios from 'axios'


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
        name="Home" 
        component={HomeScreen} 
        options={{ 
          title: 'Contacts List',
          headerTitleAlign:"center",
          headerTintColor: 'black',
           }}/>
        <Stack.Screen 
        name="AddUser" 
        component={AddScreen} 
        options={{ 
          title: 'Add Contact',
          headerTitleAlign:"center",
          headerTintColor: 'black',
           }}/>
        <Stack.Screen 
        name="UserDetail" 
        component={UserDetailScreen} 
        options={{ 
          title: 'Contact Details', 
          headerTitleAlign:"center",
          headerTintColor: 'black',
           }}/>

      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
