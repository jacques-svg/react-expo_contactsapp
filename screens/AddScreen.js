import React, { useState, useEffect } from 'react';
import { StyleSheet, Text,ScrollView,TextInput,
  View, Button} from 'react-native';
import Axios from 'axios'

export default function Add({navigation}) {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [isValid, setIsValid] = useState(true)

  const emailIsValid = (email) => {
    console.log('Regex',email);
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email) 
  }

const storeUser = () => {

  if(firstName.length <= 0){
    alert('The first Name must be at least set');
    setIsValid(false)
  }
  else if(lastName.length <= 0){
    alert('The last Name must be at least set');
    setIsValid(false)
  }
  else if(phone.length <= 0){
    alert('The phone Number must be at least set');
    setIsValid(false)

  }
  else if(email.length <= 0 ){
    alert('The email must be at least set');
  }
  else{

    if(!emailIsValid(email)){
      alert('The email format is not valid');
    }
    else{
      Axios
      .post('http://localhost:3001/api/insert', {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone
      });
      navigation.navigate('Home')

    }
  }
}
    return (
      <ScrollView style={styles.container}>
        <View style={styles.inputGroup}>
          <TextInput
              placeholder={'First Name'}
              value={firstName}
              onChangeText={(val) => setFirstName(val)}
          />
        </View>
        <View style={styles.inputGroup}>
          <TextInput
              placeholder={'Last Name'}
              value={lastName}
              onChangeText={(val) => setLastName(val)}
          />
        </View>

        <View style={styles.inputGroup}>
          <TextInput
              multiline={true}
              numberOfLines={4}
              placeholder={'Email'}
              value={email}
              onChangeText={(val) => setEmail(val)}
          />
        </View>
        <View style={styles.inputGroup}>
          <TextInput
              placeholder={'Phone'}
              value={phone}
              onChangeText={(val) => setPhone(val)}
          />
        </View>

        <View style={styles.button}>
          <Button
            title='Add User'
            onPress={() => storeUser()} 
            color="#19AC52"
          />
        </View>
      </ScrollView>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 35
    },
    inputGroup: {
      flex: 1,
      padding: 0,
      marginBottom: 15,
      borderBottomWidth: 1,
      borderBottomColor: '#cccccc',
    },
    preloader: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })

