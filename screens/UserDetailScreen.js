import React, { useState, useEffect } from 'react';
import { StyleSheet,Alert,TextInput, Text, View, Button,ActivityIndicator,ScrollView} from 'react-native';
import { ListItem } from 'react-native-elements'
import Axios from 'axios'

function UserDetail(props) {
    const [currentContact, setCurrentContact] = useState([])
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [currentUser, setCurrentUser] = useState('')
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {

        setCurrentUser(props.route.params.userkey)
        //console.log('currentId',props.route.params.userkey);
        Axios
           .get(`http://localhost:3001/api/current/user/${props.route.params.userkey}`)
           .then((response) => {
          setFirstName(response.data[0].firstName);
          setLastName(response.data[0].LastName);
          setEmail(response.data[0].email);
          setPhone(response.data[0].phone);
          setIsLoading(false);
          //console.log('response',response.data[0]);
       })

   }, [])

  const updateUser = () => {
    Axios
    .put('http://localhost:3001/api/update',{
      id:currentUser,
      firstName: firstName,
      lastName: lastName,
      email: email,
      phone: phone
    }).
    then((response) => {
    //console.log('response',response.data)
    props.navigation.navigate('Home');
    }) 

  }

  const deleteUser=()=>{
    Axios.delete(`http://localhost:3001/api/delete/${currentUser}`)
    .then((response) => {
      //console.log(response.data)
      props.navigation.navigate('Home');
    })
  }

  const openTwoButtonAlert = () =>{

    Alert.alert(
      'Delete User',
      'Are you sure?',
      [
        {text: 'Yes', onPress: () => deleteUser()},
        {text: 'No', onPress: () => console.log('No item was removed'), style: 'cancel'},
      ],
      { 
        cancelable: true 
      }
    );
  }

    if(isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
        <ScrollView style={styles.container}>
          <View style={styles.inputGroup}>
            <TextInput
                placeholder={'First Name'}
                value={firstName}
                onChangeText={(val) => setFirstName(val)}
            />
          </View>
          <View style={styles.inputGroup}>
            <TextInput
                placeholder={'Last Name'}
                value={lastName}
                onChangeText={(val) => setLastName(val)}
            />
          </View>
  
          <View style={styles.inputGroup}>
            <TextInput
                multiline={true}
                numberOfLines={4}
                placeholder={'Email'}
                value={email}
                onChangeText={(val) => setEmail(val)}
            />
          </View>
          <View style={styles.inputGroup}>
            <TextInput
                placeholder={'Phone'}
                value={phone}
                onChangeText={(val) => setPhone(val)}
            />
          </View>
  
          <View style={styles.button}>
            <Button
              title='Update'
              onPress={() => updateUser()} 
              color="#19AC52"
            />
          </View>
          <View>
          <Button
            title='Delete'
            onPress={()=> deleteUser()}
            color="#E37399"
          />
        </View>

        </ScrollView>
      );
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 35
    },
    inputGroup: {
      flex: 1,
      padding: 0,
      marginBottom: 15,
      borderBottomWidth: 1,
      borderBottomColor: '#cccccc',
    },
    preloader: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
  export default UserDetail;