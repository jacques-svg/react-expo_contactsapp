import React, { useState, useEffect } from 'react';
import { StyleSheet,TextInput,Dimensions, Text, View, Button,TouchableOpacity, ActivityIndicator,ScrollView} from 'react-native';
import { ListItem } from 'react-native-elements'
import Axios from 'axios'

const {width,height} = Dimensions.get('window');

function Home(props) {

    const [contacts, setContacts] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [search, setSearch] = useState('')


    useEffect(() => {
         Axios
            .get('http://localhost:3001/api/contacts').
        then((response) => {
            setContacts(response.data);
            setIsLoading(false);
        }) 
    }, [])

    const searchUser=(val)=>{
      setSearch(val);
      console.log(search);
    }
  

    if(isLoading){
        return(
          <View style={styles.preloader}>
            <ActivityIndicator size="large" color="#9E9E9E"/>
          </View>
        )
    } 
    return (
        <View style={styles.container,styles.text}>
          <ScrollView style={styles.containerScroll}>
            {
              contacts.map((item) => {
                return (
                  <View key={item.id} style={styles.contact}>
                    <TouchableOpacity onPress={() => {
                      props.navigation.navigate('UserDetail', {
                        userkey: item.id
                      });
                    }}>
                      <Text style={styles.text1}>{item.firstName}</Text>
                      <Text style={styles.text2}>{item.phone}</Text>
                    </TouchableOpacity>
                  </View>

                );
              })
            }
          </ScrollView>
          <View style={styles.button,styles.btn}>
            <Button
              title='Add User'
              onPress={() => props.navigation.navigate('AddUser')} 
              color="#19AC52"
            />
          </View>
        </View>   
  );
}

  const styles = StyleSheet.create({
    container: {
     flex: 1,
     paddingBottom: 10,
     padding:10
    },
    containerScroll:{
      paddingTop:90
    },
    containerInput:{
      position:"absolute",
      top:20,
      left:5
    },
    inputContainer:{
      flexDirection:"row",
      justifyContent:"space-between",
      alignItems:"center",
      padding:10,
      backgroundColor:"#fff",
      borderRadius:8,
      width:width - 50,
      marginRight:100,
  },
  input:{
    fontSize:16,
    color:"#303030",
    maxWidth:"70%",
    minWidth:"30%",
    fontFamily:"Poppins"
},
    contact:{
      //borderWidth:0.3,
      justifyContent: 'center',
      textAlign: 'left',
      padding:10
    },
    inputGroup: {
      flex: 1,
      padding: 0,
      marginBottom: 15,
      borderBottomWidth: 1,
      borderBottomColor: '#cccccc',
      height:10
    },
    text1:{
      fontSize: 16,
      fontWeight: "bold"    
    },
    text2:{
      fontSize: 13,
      paddingLeft:5
    },

    btn:{
      width:110,
      padding:10,
    },
    buttonText: {
      fontSize: 20,
      color: '#fff',
    },
    preloader: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })

  export default Home;